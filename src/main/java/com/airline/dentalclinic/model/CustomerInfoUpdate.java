package com.airline.dentalclinic.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerInfoUpdate {

    private String customerName;
    private String customerPhone;
}
