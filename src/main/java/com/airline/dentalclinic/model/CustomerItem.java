package com.airline.dentalclinic.model;

import com.airline.dentalclinic.enums.Gender;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CustomerItem {
    private Long id;
    private String customerName;
    private String customerPhone;
    private String gender;
    private Integer age;
    private LocalDate firstVisit;
    private LocalDate lastVisit;
    private String statusName;

    private Long elapsedDateCount;
}
