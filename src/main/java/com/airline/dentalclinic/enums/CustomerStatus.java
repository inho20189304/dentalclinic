package com.airline.dentalclinic.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CustomerStatus {
    READY("대기"),
    VISIT_REQUEST("방문 요망"),
    EMERGENCY("긴급"),
    QUIESCENCE("휴먼");
    private final String statusName;
}
