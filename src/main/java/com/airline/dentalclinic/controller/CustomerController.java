package com.airline.dentalclinic.controller;

import com.airline.dentalclinic.model.CustomerInfoUpdate;
import com.airline.dentalclinic.model.CustomerItem;
import com.airline.dentalclinic.model.CustomerRegister;
import com.airline.dentalclinic.service.CustomerService;
import lombok.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid CustomerRegister register) {
        customerService.setCustomer(register);

        return "ok";
    }
    @GetMapping("/all")
    public List<CustomerItem> getCustomers() {
        List<CustomerItem> result = customerService.getCustomers();

        return result;
    }
    @PutMapping("/update/id/{id}")
    public String putCustomerInfoUpdate(@PathVariable long id, @RequestBody @Valid CustomerInfoUpdate update) {
        customerService.putCustomerInfoUpdate(id, update);

        return "ok";
    }
    @PutMapping("/visit/id/{id}")
    public String putCustomerVisit(@PathVariable long id){
        customerService.putCustomerVisit(id);

        return "ok";
    }
}