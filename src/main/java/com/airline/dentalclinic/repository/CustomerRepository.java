package com.airline.dentalclinic.repository;

import com.airline.dentalclinic.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
