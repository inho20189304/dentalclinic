package com.airline.dentalclinic.service;

import com.airline.dentalclinic.entity.Customer;
import com.airline.dentalclinic.enums.CustomerStatus;
import com.airline.dentalclinic.model.CustomerInfoUpdate;
import com.airline.dentalclinic.model.CustomerItem;
import com.airline.dentalclinic.model.CustomerRegister;
import com.airline.dentalclinic.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomer(CustomerRegister register) {
        Customer addData = new Customer();
        addData.setCustomerName(register.getCustomerName());
        addData.setCustomerPhone(register.getCustomerPhone());
        addData.setBirthday(register.getBirthday());
        addData.setAge(register.getAge());
        addData.setGender(register.getGender());
        addData.setFirstVisit(LocalDate.now());
        addData.setLastVisit(LocalDate.now());

        customerRepository.save(addData);
    }
    public List<CustomerItem> getCustomers() {
        List<Customer>  originList  = customerRepository.findAll();
        List<CustomerItem> result = new LinkedList<>();

        for (Customer item : originList) {
            CustomerItem addItem = new CustomerItem();
            addItem.setId(item.getId());
            addItem.setCustomerName(item.getCustomerName());
            addItem.setCustomerPhone(item.getCustomerPhone());
            addItem.setGender(item.getGender().getName());
            addItem.setAge(item.getAge());
            addItem.setFirstVisit(item.getFirstVisit());
            addItem.setLastVisit(item.getLastVisit());

            LocalDateTime timeStart = LocalDateTime.of(
                    item.getLastVisit().getYear(),
                    item.getLastVisit().getMonthValue(),
                    item.getLastVisit().getDayOfMonth(),
                    0,
                    0,
                    0
            );

            LocalDateTime timeEnd = LocalDateTime.now();

            long dayCount = ChronoUnit.DAYS.between(timeStart, timeEnd);
            addItem.setElapsedDateCount(dayCount);

            CustomerStatus customerStatus = CustomerStatus.QUIESCENCE;
            if (dayCount <= 30) {
                customerStatus = CustomerStatus.READY;
            } else if (dayCount <= 90) {
                customerStatus = CustomerStatus.VISIT_REQUEST;
            } else if (dayCount <= 365) {
                    customerStatus = CustomerStatus.EMERGENCY;
                }
                addItem.setStatusName(customerStatus.getStatusName());

            result.add(addItem);
        }
        return result;
    }
    public void putCustomerInfoUpdate(long id, CustomerInfoUpdate update) {
        Customer originData = customerRepository.findById(id).orElseThrow();
        originData.setCustomerName(update.getCustomerName());
        originData.setCustomerPhone(update.getCustomerPhone());

        customerRepository.save(originData);
    }
    public void putCustomerVisit(long id) {
        Customer originData = customerRepository.findById(id).orElseThrow();
        originData.setLastVisit(LocalDate.now());

        customerRepository.save(originData);
    }
}
