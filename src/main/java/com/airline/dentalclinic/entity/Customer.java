package com.airline.dentalclinic.entity;

import com.airline.dentalclinic.enums.Gender;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String customerName;
    @Column(nullable = false, length = 20)
    private String customerPhone;
    @Column(nullable = false, length = 5)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;
    @Column(nullable = false)
    private LocalDate birthday;
    @Column(nullable = false)
    private Integer age;
    @Column(nullable = false)
    private LocalDate firstVisit;
    private LocalDate lastVisit;
}
